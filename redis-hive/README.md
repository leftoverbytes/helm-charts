# redis-hive

Helm chart for redis-hive.

_More information on redis-hive can be found [here](https://gitlab.com/leftoverbytes/redis-hive)._

## Installation

```
helm repo add leftoverbytes https://leftoverbytes.com/charts
helm repo update
helm install redis-hive leftoverbytes/redis-hive
```

## Values


| Name | Description | Default |
| ------ | ------ | ------ |
| replicas | As a statefulset or a deployment, how many replicas. | 3 |
| hive.image.name | Docker image name for the redis-hive container. | registry.gitlab.com/leftoverbytes/redis-hive |
| hive.image.tag | Docker tag for redis-hive. | v0.0.2 |
| hive.controller.port | Port for the hive controller, which governs membership. | 3000 |
| hive.proxy.port | Port for the hive proxy, which is the back-end for the master service. | 7000 |
| redis.image.name | Docker image name for redis. | redis |
| redis.image.tag | Docker tag for redis. | 5-alpine |
| redis.port | Port for the redis replica service, which can be used by cluster applications. | 6379 |
| redis.internalPort | Container port for internal redis traffic. | 6379 |
| redis.persistence.enabled | Whether to use PVCs for redis persistence. A true value use statefulset instead of deployment. | false |
| redis.persistence.storageClass | Cluster storage class for PVCs. | default |
| redis.persistence.size | PVC size to be allocated. | 1Gi |
| redis.auth.enabled | Whether to use redis authentication. The same value will be used for `requirepass` and `masterauth` config values. | false |
| redis.auth.password | Specify the password as a value. | |
| redis.auth.secret | Alternately, specify the pre-existing secret to use for the password. | |
| redis.auth.secretKey | The key within the pre-existing secret. | |

